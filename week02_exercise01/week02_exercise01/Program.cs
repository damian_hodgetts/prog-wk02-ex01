﻿using System;

namespace week02_exercise01
{
	class MainClass
	{
		public static void Main(string[] args)
		{
			var number1 = 3;
			var number2 = 5;

			if (number1 > number2) {
				Console.WriteLine("Number 1 is bigger than Number 2");
			}
			else {
				Console.WriteLine("Number 1 is smaller or equal to Number 2");
			}
		}
	}
}
